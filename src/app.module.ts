import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { ConfigsModule } from './configs/configs.module';
import { PaymentsModule } from './payments/payments.module';
import { OrdersModule } from './orders/orders.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { ProductsModule } from './products/products.module';

@Module({
  imports: [
      ConfigModule.forRoot({ isGlobal: true, envFilePath: `${process.env.NODE_ENV ? process.env.NODE_ENV : 'local'}.env` }),
      ConfigsModule,
      PaymentsModule,
      OrdersModule,
      AuthModule,
      UsersModule,
      ProductsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
