import { BadRequestException, Inject, Injectable, Scope } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';

@Injectable({ scope: Scope.REQUEST })
export class PaymentsService {
    constructor(
        @Inject(REQUEST) private request: Request,
        @Inject('PAYMENT_SERVICE') private client: ClientProxy) {
    }

    async create(body): Promise<any> {
        const pattern = { cmd: 'payments-create' };
        return await this.client.send<any>(pattern, {
            body: body
        }).toPromise().then((response) => {
            return response;
        }).catch((error) => {
            throw new BadRequestException(error)
        })
    }

    async backdoor(body): Promise<any> {
        const pattern = { cmd: 'payments-backdoor' };
        return await this.client.send<any>(pattern, {
            body: body
        }).toPromise().then((response) => {
            return response;
        }).catch((error) => {
            console.log('error', error);
            throw new BadRequestException(error)
        })
    }

    async update(id, data): Promise<any> {
        const pattern = { cmd: 'payments-update' };
        return await this.client.send<any>(pattern, { id, body: data }).toPromise().then((response) => {
            return response;
        }).catch((error) => {
            throw new BadRequestException(error)
        })
    }

    async find(query): Promise<any> {
        const pattern = { cmd: 'payments-find' };

        return this.client.send<any>(pattern, {
            query: query
        }).toPromise().then((response) => {
            return response;
        }).catch((error) => {
            throw new BadRequestException(error)
        })
    }

    async findOne(params): Promise<any> {
        const pattern = { cmd: 'payments-find-one' };

        return await this.client.send<any>(pattern, {
            params: params
        }).toPromise().then((response) => {
            return response;
        }).catch((error) => {
            throw new BadRequestException(error)
        })
    }
}
