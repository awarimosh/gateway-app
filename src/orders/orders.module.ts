import { Module } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { OrdersController } from './orders.controller';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [
    ClientsModule.register([{name: 'ORDER_SERVICE', transport: Transport.TCP, options: {port: 3001}}]),
  ],
  providers: [OrdersService,],
  controllers: [OrdersController]
})
export class OrdersModule { }
