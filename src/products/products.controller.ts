import {Body, Controller, Delete, Get, Param, Post, Put, Query} from '@nestjs/common';
import {ProductsService} from "./products.service";

@Controller('products')
export class ProductsController {
    constructor(private productsService: ProductsService) {
    }

    @Post()
    async create(@Body() data:any) :Promise<any> {
        return await this.productsService.create(data);
    }

    @Get()
    async find(@Query() query:any): Promise<any> {
        return await this.productsService.find(query);
    }

    @Get(':id')
    async findOne(@Param() params:any): Promise<any> {
        return await this.productsService.findOne(params);
    }

    @Put(':id')
    async update(@Param() params: any, @Body() body: any): Promise<any> {
        return await this.productsService.update(params, body);
    }

    @Delete(':id')
    async delete(@Param() params:any): Promise<any> {
        return await this.productsService.delete(params);
    }
}
