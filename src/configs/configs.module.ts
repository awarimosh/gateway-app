import { Module } from '@nestjs/common';
import { ConfigsService } from './configs.service';

@Module({
  providers: [
    {
      provide: ConfigsService,
      useValue: new ConfigsService(`${process.env.NODE_ENV ? process.env.NODE_ENV : 'local'}.env`),
    },
  ],
  exports: [ConfigsService],
})
export class ConfigsModule { }
