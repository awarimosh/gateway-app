import { Module } from '@nestjs/common';
import { PaymentsController } from './payments.controller';
import { PaymentsService } from './payments.service';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { ConfigsModule } from 'src/configs/configs.module';
@Module({
  imports: [
    ConfigsModule,
    ClientsModule.register([{ name: 'PAYMENT_SERVICE', transport: Transport.TCP, options: { port: 3002 } }]),
  ],
  controllers: [PaymentsController],
  providers: [PaymentsService]
})
export class PaymentsModule { }
