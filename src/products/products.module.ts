import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [
      ClientsModule.register([{name: 'ORDER_SERVICE', transport: Transport.TCP, options: {port: 3001}}])
  ],
  providers: [ProductsService],
  controllers: [ProductsController]
})
export class ProductsModule {}
