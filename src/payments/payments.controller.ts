import { Controller, UseGuards, Post, Get, Put, Body, Query, Param } from '@nestjs/common';
import { PaymentsService } from './payments.service';
import { AuthGuard } from '@nestjs/passport';
import { Redirect } from "@nestjsplus/redirect";

@Controller('payments')
export class PaymentsController {
    constructor(private paymentsService: PaymentsService) {
    }

    @Post()
    //@UseGuards(AuthGuard('jwt'))
    async create(@Body() body: any): Promise<any> {
        return await this.paymentsService.create(body);
    }

    @Redirect()
    @Post("backdoor")
    async backdoor(@Body() body: any): Promise<any> {
        return await this.paymentsService.backdoor(body);
    }

    @Get()
    //@UseGuards(AuthGuard('jwt'))
    async find(@Query() query: any): Promise<any> {
        return await this.paymentsService.find(query);
    }

    @Get(":id")
    //@UseGuards(AuthGuard('jwt'))
    async findOne(@Param() params: any): Promise<any> {
        return await this.paymentsService.findOne(params);
    }

    @Put(":id")
    //@UseGuards(AuthGuard('jwt'))
    async update(@Param('id') params: any, @Body() data: any): Promise<any> {
        return await this.paymentsService.update(params, data);
    }
}
