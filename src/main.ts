import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.connectMicroservice({ transport: Transport.TCP, options: { retryAttempts: 5, retryDelay: 3000 }, });
    app.useGlobalPipes(new ValidationPipe({ disableErrorMessages: false, transform: true, }));

    await app.startAllMicroservicesAsync();
    app.enableCors({
        allowedHeaders: 'Authorization,Content-Type,user',
        origin: "*",
        methods: ['GET', 'HEAD', 'PUT', 'POST', 'DELETE'],
        optionsSuccessStatus: 200
    });
    await app.listen(3000)
}
bootstrap();