import { Controller, Body, Post, Get, UseGuards, Put, Delete, Query, Param } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('orders')
export class OrdersController {
    constructor(private ordersService: OrdersService) {
    }

    @Post()
    //@UseGuards(AuthGuard('jwt')) 
    async create(@Body() data: any): Promise<any> {
        return await this.ordersService.create(data);
    }

    @Get()
    //@UseGuards(AuthGuard('jwt')) 
    async find(@Query() query: any): Promise<any> {
        return await this.ordersService.find(query);
    }

    @Get(":id")
    //@UseGuards(AuthGuard('jwt'))
    async findOne(@Param() params: any): Promise<any> {
        return await this.ordersService.findOne(params);
    }

    @Put(":id")
    //@UseGuards(AuthGuard('jwt'))
    async update(@Param() params: any, @Body() body: any): Promise<any> {
        return await this.ordersService.update(params, body);
    }

    @Delete(":id")
    //@UseGuards(AuthGuard('jwt'))
    async delete(@Param() params: any): Promise<any> {
        return await this.ordersService.delete(params);
    }
}
