import { BadRequestException, Inject, Injectable, Scope } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';

@Injectable()
export class ProductsService {
    constructor(
        @Inject(REQUEST) private request: Request,
        @Inject('ORDER_SERVICE') private client: ClientProxy,) {
    }

    async create(body): Promise<any> {
        const pattern = {cmd: 'products-create'};
        return await this.client.send<any>(pattern, {
            body: body
        }).toPromise().then((response) => {
            return response;
        }).catch((error) => {
            throw new BadRequestException(error)
        })
    }

    async find(query): Promise<any> {
        const pattern = {cmd: 'products-find'};
        console.log('products-find');
        return this.client.send<any>(pattern, {
            query: query
        }).toPromise();
    }

    async findOne(params): Promise<any> {
        const pattern = {cmd: 'products-find-one'};
        return await this.client.send<any>(pattern, { params: params })
            .toPromise().then((response) => {
                return response;
            }).catch((error) => {
                throw new BadRequestException(error)
            })
    }

    async update(params, body): Promise<any> {
        const pattern = {cmd: 'products-update'};
        return await this.client.send<any>(pattern, {
            params: params,
            body: body
        }).toPromise().then((response) => {
            return response;
        }).catch((error) => {
            throw new BadRequestException(error)
        })
    }

    async delete(params): Promise<any> {
        const pattern = { cmd: 'products-delete' };
        return await this.client.send<any>(pattern, { id: params.id }).toPromise().then((response) => {
            return response;
        }).catch((error) => {
            throw new BadRequestException(error)
        })
    }
}
